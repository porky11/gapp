pub trait Render<R> {
    fn render(&self, renderer: &mut R);
}

pub trait Update {
    fn update(&mut self, timestep: f32);
}

impl<R, T: Render<R>> Render<R> for Vec<T> {
    fn render(&self, renderer: &mut R) {
        for app in self {
            app.render(renderer);
        }
    }
}

impl<T: Update> Update for Vec<T> {
    fn update(&mut self, timestep: f32) {
        for app in self {
            app.update(timestep);
        }
    }
}
